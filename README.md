# AWSCLI Container

The AWSCLI container is to be used as part of the 3 Musketeers pattern of CICD. Docker, Compose & Make.

In order for this to work, the container needs access to AWS credentials either via an env file, or by placing a custom script into /scripts.


